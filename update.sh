#!/bin/bash

# Define path (necessary to schedule tasks with cron)
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Broadcast messages and perform updates
mscs broadcast /say Starting monthly system updates. The server may run a bit slower while these are running.
sudo apt update
sudo apt -y upgrade
mscs broadcast /say Updates complete. The system will restart at 2 AM MST, approx. 1 hour from now.

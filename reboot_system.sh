#!/bin/bash

# Define path (necessary to schedule tasks with cron)
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Broadcast messages and perform system restart
mscs broadcast /say Inintializing monthly system restart. This restart may take a few minutes.
mscs stop
sudo shutdown -r now

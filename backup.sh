#!/bin/bash

# Define path (necessary to schedule tasks with cron)
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Broadcast messages and perform backup
mscs broadcast /say Startings scheduled world backup. The server may run a bit slower while this is running.
mscs backup
mscs broadcast /say Backup complete.

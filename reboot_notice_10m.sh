#!/bin/bash

# Define path (necessary to schedule tasks with cron)
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Broadcast message
mscs broadcast /say The server will restart in 10 minutes.

#!/bin/bash

# Define path (necessary to schedule tasks with cron)
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Start the server
mscs start

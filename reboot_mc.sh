#!/bin/bash

# Define path (necessary to schedule tasks with cron)
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Perform server restart (it will automatically wait one minute and give a notice in server chat)
mscs restart

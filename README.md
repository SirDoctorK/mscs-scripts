# MSCS scripts

Simple shell scripts for use with MSCS (Minecraft Server Control Script). These are all very short, but can make your Minecraft server maintenance a little bit easier.

## Prerequesites

These scripts interact with MSCS. You will need to get MSCS from [GitHub](https://github.com/MinecraftServerControl/mscs).

Also, the update scripts are written to update Ubuntu-based distributions. If your Minecraft server is running on a Red Hat-based distribution, you will not be able to use the update scripts unless you modify them.

## Crontab

These scripts are designed to be executed using Cron jobs. Included is an example crontab file demonstrating how these scripts may be scheduled to run regularly.